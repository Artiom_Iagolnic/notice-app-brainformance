<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        try {
            /**
             * @var User $user
             */

            $notes = auth()->user()->notes;

            return response()->json([
                'code' => 200,
                'message' => 'Success',
                'data' => $notes
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Internal Server Error',
                'data' => null,
                'error' => $e->getMessage()
            ]);

        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NoteRequest $request): JsonResponse
    {
        try {
            /**
             * @var User $user
             */
            $user = auth()->user();

            $note = $user->notes()->create($request->all());

            return response()->json([
                'code' => 201,
                'message' => 'Note created',
                'data' => $note
            ]);
        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Server Error',
                'data' => null,
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NoteRequest $request, string $id): JsonResponse
    {
        try {
            /**
             * @var User $user
             */
            $user = auth()->user();

            $note = $user->notes()->find($id);

            if (!$note) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Note not found',
                    'data' => null
                ], 404);
            }

            $note->update($request->all());

            return response()->json([
                'code' => 200,
                'message' => 'Note updated',
                'data' => $note
            ]);
        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Server Error',
                'data' => null,
                'error' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        try {
            /**
             * @var User $user
             */
            $user = auth()->user();

            $note = $user->notes()->find($id);

            if (!$note) {
                return response()->json([
                    'code' => 404,
                    'message' => 'Note not found',
                    'data' => null
                ], 404);
            }

            $note->delete();

            return response()->json([
                'code' => 200,
                'message' => 'Note deleted',
                'data' => null
            ]);
        } catch (Exception $e) {
            return response()->json([
                'code' => 500,
                'message' => 'Server Error',
                'data' => null,
                'error' => $e->getMessage()
            ], 500);
        }
    }
}
