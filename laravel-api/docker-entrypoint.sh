#!/bin/sh

# Wait for MySQL to be ready
until mysql -h"${DB_HOST}" -u"${DB_USERNAME}" -p"${DB_PASSWORD}" -e "SHOW DATABASES;" > /dev/null 2>&1; do
  echo "Waiting for MySQL..."
  sleep 3
done

php artisan key:generate
# Run migrations
php artisan migrate --seed

# Start Laravel development server
exec "$@"
