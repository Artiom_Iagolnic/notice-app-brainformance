// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    compatibilityDate: '2024-04-03',
    devtools: {enabled: true},
    modules: ["@nuxt/image", "@nuxtjs/tailwindcss", "shadcn-nuxt", '@pinia/nuxt'],
    shadcn: {
        prefix: '',
        componentDir: './components/ui'
    },
    runtimeConfig: {
        public: {
            apiBase: process.env.API_BASE_URL || 'http://localhost:8000',
            credentials: 'include',
        },
    },
    pinia: {
        storesDirs: ['~/store/**']
    },
    typescript: {
        typeCheck: false
    },
    imports: {
        dirs: [
            'composables'
        ]
    }
})