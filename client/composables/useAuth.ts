import {useUserStore} from "~/store/user";
import {useLoadingStore} from "~/store/loading";
import {useErrorStore} from "~/store/errors";
import {apiRequest} from '~/utils/apiUtils';

export const useAuth = () => {
    const isLoading = useLoadingStore();
    const router = useRouter();
    const userStore = useUserStore();
    const errorStore = useErrorStore();

    const login = async (email: string, password: string) => {
        isLoading.setLoading(true);
        errorStore.clearErrors();
        try {
            const response = await apiRequest('/login', 'POST', {email, password});
            if (response && response.token && response.user) {
                userStore.setUser(response.user as { name: string; email: string });
                localStorage.setItem('authToken', response.token);
                await router.push('/');
            } else {
                throw new Error('Invalid login response');
            }
        } catch (e: any) {
            handleApiError(e);
        } finally {
            isLoading.setLoading(false);
        }
    };

    const logout = async () => {
        isLoading.setLoading(true);
        errorStore.clearErrors();
        try {
            await apiRequest('/logout', 'POST');
            userStore.setUser(null);
            localStorage.removeItem('authToken');
            await router.push('/login');
        } catch (e) {
            handleApiError(e);
        } finally {
            isLoading.setLoading(false);
        }
    };

    const getUser = async () => {
        errorStore.clearErrors();
        const response = await apiRequest('/user', 'GET');
        if (response && response.user) {
            userStore.setUser(response.user as { name: string; email: string });
        }
    };

    const register = async (name: string, email: string, password: string, password_confirmation: string) => {
        isLoading.setLoading(true);
        errorStore.clearErrors();
        try {
            const response = await apiRequest('/register', 'POST', {name, email, password, password_confirmation});
            if (response && response.token && response.user) {
                userStore.setUser(response.user as { name: string; email: string });
                localStorage.setItem('authToken', response.token);
                await router.push('/');
            } else {
                throw new Error('Invalid register response');
            }
        } catch (e: any) {
            handleApiError(e);
        } finally {
            isLoading.setLoading(false);
        }
    };

    const handleApiError = (e: any) => {
        if (e.response && e.response._data) {
            const errors = e.response._data.errors || {general: [e.response._data.message || 'An error occurred']};
            errorStore.setFieldErrors(errors);
        } else if (e.message) {
            errorStore.setFieldErrors({general: [e.message]});
        } else {
            errorStore.setFieldErrors({general: ['An unknown error occurred.']});
        }
    };

    return {login, logout, getUser, register};
};