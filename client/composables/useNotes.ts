import {useNotesStore} from '~/store/notes';
import {apiRequest} from '~/utils/apiUtils';
import type {Note} from "~/types/note";
import {useErrorStore} from '~/store/errors';

export const useNotes = () => {
    const notesStore = useNotesStore();
    const errorStore = useErrorStore();

    interface APIResponse {
        data: Note[];
    }

    const getNotes = async () => {
        errorStore.clearErrors();
        try {
            const response = await apiRequest('/api/notes', 'GET');
            if (response) {
                notesStore.setNotes(response.data as Note[]);
            }
        } catch (e: any) {
            handleapiError(e);
        }
    };

    const addNote = async (note: { title: string; content: string }) => {
        errorStore.clearErrors();
        try {
            await apiRequest('/api/notes', 'POST', note);
            await getNotes();
        } catch (e: any) {
            handleapiError(e);
        }
    };

    const updateNote = async (note: { id: number; title: string; content: string }) => {
        errorStore.clearErrors();
        try {
            await apiRequest(`/api/notes/${note.id}`, 'PUT', note);
            await getNotes();
        } catch (e: any) {
            handleapiError(e);
        }
    };

    const deleteNote = async (id: number) => {
        errorStore.clearErrors();
        try {
            await apiRequest(`/api/notes/${id}`, 'DELETE');
            await getNotes();
        } catch (e: any) {
            handleapiError(e);
        }
    };
    const handleapiError = (e: any) => {
        if (e.response && e.response._data) {
            const errors = e.response._data.errors || {general: [e.response._data.message || 'An error occurred']};
            errorStore.setFieldErrors(errors);
        } else if (e.message) {
            errorStore.setFieldErrors({general: [e.message]});
        } else {
            errorStore.setFieldErrors({general: ['An unknown error occurred.']});
        }
    };

    return {
        getNotes,
        addNote,
        updateNote,
        deleteNote,
    };
};