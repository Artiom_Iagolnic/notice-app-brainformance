export const API_BASE_URL = process.env.API_BASE_URL || 'http://localhost:8000';
export const FRONTEND_URL = process.env.NUXT_PUBLIC_FRONTEND_URL || 'http://localhost:3000';
export const CSRF_COOKIE_NAME = process.env.CSRF_COOKIE_NAME || 'XSRF-TOKEN';
export const CSRF_HEADER_NAME = process.env.CSRF_HEADER_NAME || 'X-XSRF-TOKEN';