import {API_BASE_URL, CSRF_COOKIE_NAME, CSRF_HEADER_NAME} from './api.config';
import type {ApiResponse} from '~/types/apiResponse';

type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';
export const getToken = async () => {
    await $fetch('/sanctum/csrf-cookie', {
        baseURL: API_BASE_URL,
        credentials: 'include',
    });
    const tokenMatch = document.cookie.match(new RegExp(`(^| )${CSRF_COOKIE_NAME}=([^;]+)`));
    return tokenMatch ? decodeURIComponent(tokenMatch[2]) : null;
};

export const apiRequest = async <T>(url: string, method: HttpMethod, body?: any) => {
    const csrfToken = await getToken();
    const response: ApiResponse<T> = await $fetch(url, {
        method: method,
        baseURL: API_BASE_URL,
        body: body,
        credentials: 'include',
        headers: {
            [CSRF_HEADER_NAME]: csrfToken || '',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    });
    return response;
};