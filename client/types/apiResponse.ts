export interface ApiResponse<T = any>{
    token: string;
    user: Object;
    data: T;
    message?: string;
    errors?: { [key: string]: string[] };
}