import type {Note} from '~/types/note';

export const useNotesStore = defineStore('notes', {
    state: () => ({
        notes: [] as Note[],
        originalNotes: [] as Note[],
    }),
    actions: {
        setNotes(data: Note[]) {
            this.notes = data;
            this.originalNotes = data;
        },
        filterNotes(filter: string) {
            if (!filter) {
                this.notes = this.originalNotes;
            } else {
                this.notes = this.originalNotes.filter(note =>
                    note.title.toLowerCase().includes(filter.toLowerCase()) ||
                    note.content.toLowerCase().includes(filter.toLowerCase())
                );
            }
        },
    },
});