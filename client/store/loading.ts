
export const useLoadingStore = defineStore('isLoading', {
    state: () => ({
        isLoading: true,
    }),
    actions: {
        setLoading(data: boolean) {
            this.$patch({isLoading: data})
        }
    }
});