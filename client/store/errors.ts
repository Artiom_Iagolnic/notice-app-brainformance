import {defineStore} from 'pinia';

export const useErrorStore = defineStore('errors', {
    state: () => ({
        fieldErrors: {} as Record<string, string[]>,
    }),
    actions: {
        setFieldErrors(errors: Record<string, string[]>) {
            this.fieldErrors = errors;
        },
        clearErrors() {
            this.fieldErrors = {};
        },
        getErrorMessages(field: string) {
            return this.fieldErrors[field] || [];
        },
    },
});