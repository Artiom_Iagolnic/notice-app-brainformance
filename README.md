# notice-app-brainformance

## Getting started

## Start from Docker
```
git clone https://gitlab.com/Artiom_Iagolnic/notice-app-brainformance.git
cd notice-app-brainformance
cd laravel-api

cp .env.example .env

change DB_HOST=127.0.0.1 to DB_HOST=mysql in .env file

From root directory run:

docker-compose up --build
```

## Start from Git
```
git clone https://gitlab.com/Artiom_Iagolnic/notice-app-brainformance.git
cd notice-app-brainformance
```

## Setup intstructions

Follow these steps to set up the project on your local machine.

### Prerequisites

	•	Node.js  (version 20 or higher)
	•	NPM or Yarn
	•	PHP  (version 8.3 or higher)
	•	Composer
	•	MySQL

### Frontend Setup

navigate to the frontend directory:
``` cd client ```

#### Install dependencies:

npm install

#### Create an environment file:

        cp .env.example .env

Update the environment variables in the .env file.

#### Run the development server:

    npm run dev

## Backend Setup

Create database on db server

navigate to the backend directory:
``` cd laravel-api ```

#### Install dependencies:

composer install

#### Create an environment file:

    cp .env.example .env

Update the environment variables in the .env file.

#### Generate an application key:

    php artisan key:generate

#### Run the database migrations:

    php artisan migrate

#### Seed the database:

    php artisan db:seed

#### Run the development server:

    php artisan serve

## Environment Variables

#### Frontend

```API_BASE_URL: Base URL of the backend API```

#### Backend

```FRONTEND_URL: URL of the frontend application```

```SANCTUM_STATEFUL_DOMAINS: Comma-separated list of stateful domains```

```DB_CONNECTION: Database connection (mysql)```

```DB_HOST: Database host```

```DB_PORT: Database port```

```DB_DATABASE: Database name```

```DB_USERNAME: Database username```

```DB_PASSWORD: Database password```

```SESSION_DOMAIN: Session domain```

```SESSION_SECURE_COOKIE: Secure session cookie (true/false)```

```SESSION_DRIVER: Session driver (e.g., database)```

```SESSION_SAME_SITE: Session same-site policy (lax/strict)```

## Name

notice-app-brainformance

## Description

Notice App is a web application for managing notes. Users can create, edit, view, and delete notes.

## Features

	•	User Authentication
	•	Create, Read, Update, and Delete (CRUD) Notes
	•	Responsive Design
	•	CSRF Protection

## Technologies

	•	Frontend: Vue.js, Nuxt.js
	•	Backend: Laravel
	•	State Management: Pinia
	•	Form Validation: Vee-Validate
	•	HTTP Client: built-in Nuxt.js HTTP client ohmyfetch
	•	Database: MySQL